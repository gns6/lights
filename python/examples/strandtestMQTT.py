#!/usr/bin/env python3
# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import paho.mqtt.client as mqtt
import time
from neopixel import *
from stepper import stepper
import argparse

# LED strip configuration:
LED_COUNT      = 294      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

# Stars 
currLitConstCodeIndex = 0
codeIndexToBoardIndex = [0, 1, 2, 3, 7, 8, 9, 36, 5, 4, 46, 47, 45, 42, 43, 41, 6, 38, 37, 35, 34,  10, 11, 30, 31, 32, 33, 39, 28, 29, 12, 13, 14, 27, 26, 16, 15, 17, 21, 22, 20, 24, 25, 40, 23, 19, 44, 18, 48]
ledIDToBoardIndex = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6,7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 7, 7, 9, 9, 9, 9, 7, 7, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 14, 14,14, 14, 14, 15, 16, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 30, 30, 30, 30, 30, 31, 31, 31, 31, 31, 31, 31, 32, 32, 32, 32, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 35, 35, 36, 36, 36, 36, 36, 36, 37, 37, 38, 38, 38, 38, 38, 38, 38, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 40, 40, 40, 40, 40, 40, 40, 41, 41, 41, 42, 42, 42, 42, 42, 42, 43, 43, 43, 43, 43, 44, 44, 44, 44, 44, 44, 45, 45, 45, 45, 45, 45, 45, 47, 46,  46, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48]
currRotation = 0

#Define MQTT functions
def on_connect(client, data, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("/cityOfStarsConst")
    client.subscribe("/cityOfStarsRotation")

def on_message(client, data, msg):
    global currLitConstCodeIndex
    global currRotation
    print(msg.topic+" "+str(msg.payload))
    if (msg.topic == "/cityOfStarsConst"):
	currLitConstCodeIndex = int(msg.payload)
        print(currLitConstCodeIndex)
    if (msg.topic =="/cityOfStarsRotation"):
#	msgg = msg.payload
#	if(int(msgg) != int(currRotation))
#	    diff = msgg-currRotation
#	    if diff >= 0:
#		numSteps = diff*200/31.749
#		myStepper.step(numSteps, "right")
#	    if diff<0:
#		numSteps = diff*-1*200/31.749
#		myStepper.step(numSteps, "left")
#	    currRotation = msgg
	myStepper.step(100, "right")
	print(currRotation)

# Define functions which animate LEDs in various ways.
def stars(strip, wait_ms=20, iterations=1):
    """ Starz baby """
    global currLitConst
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            if (ledIDToBoardIndex[i] == codeIndexToBoardIndex[currLitConstCodeIndex]):
                strip.setPixelColor(i, Color(127,0, 0))
            else:
	            strip.setPixelColor(i, Color(127,127,127))
	strip.show()
	time.sleep(wait_ms/1000.0)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, wheel((i+j) % 255))
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, 0)

# Main program logic follows:
if __name__ == '__main__':
    #Set up MQTT
    client=mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect_async("test.mosquitto.org", 1883, 60, "")
    client.loop_start()
    print("client.connect() called")

    # Set up motor
    myStepper = stepper([17, 27, 22])

 # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:

        while True:
#            print ('Color wipe animations.')
#            colorWipe(strip, Color(255, 0, 0))  # Red wipe
#            colorWipe(strip, Color(0, 255, 0))  # Blue wipe
#            colorWipe(strip, Color(0, 0, 255))  # Green wipe
#            print ('Theater chase animations.')
#            theaterChase(strip, Color(127, 127, 127))  # White theater chase
#            theaterChase(strip, Color(127,   0,   0))  # Red theater chase
#            theaterChase(strip, Color(  0,   0, 127))  # Blue theater chase
             print("Stars animation")
	     stars(strip)

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)

    #MQTT 
